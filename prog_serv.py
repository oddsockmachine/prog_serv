# A progress server wrapped around and iterable

# TODO
# Handle total=0/None better. Remove graph and x of y, just show num completed
# better looking graph - eg using bootstrap progress meter
# persistent server, show results after iterable finished
#    start server from separate python file - in module to be reported, send http to server when constructing, updating, finishing an iterable
# many graphs, same server
# use sockets for auto-updating page

from flask import Flask, render_template
# from flask.ext.socketio import SocketIO, emit
import threading
from datetime import datetime
app = Flask( __name__ )
app.debug = True
app.config['SECRET_KEY'] = 'secret!'
app.iterables = {}  # a dict of iterables/progress bars against name
app.state = "off"


@app.route('/')
def index():
    return "hi"

@app.route('/<iterable_name>')
def index(iterable_name):
    return str(iterable_name)


class Iterable(object):
    def __init__(self, name, total):
        self.name, self.total = name, total
        self.done = 0
        self.state = "ready"
        
        print "created new iterable '{}'".format(name)
        if name in app.iterables:
            print "warning: iterable with name '{}' already exists. It will be overwritten".format(name)
        app.iterables[name] = self
        return
    def inc(self):
        self.state = "doing"
        self.done += 1
      
    
  
def check_server():
    if app.state == "running":
        return
    else:
        app.state = "running"
        def target():
            app.run( host='0.0.0.0', port=1234 )
        thread = threading.Thread(target=target)
        thread.daemon = True  # Ensures thread dies when main exits
        thread.start()
        return thread


def report(iterable, name):
    # Check whether the server has been started
    # If not, start it now.
    check_server()
    try:
        total = len(iterable)
        print "total is " + str(total)
    except TypeError:
        print "unknown total"
        total = None
        
    # Create a new iterable
    _iter = Iterable(name, total)
    for obj in iterable:
        yield obj
    print "done"
    return



def main():
    app.state = "running"
    app.run(host="0.0.0.0", port=1234)
    
    


if __name__ == "__main__":
    print "!"
    main()
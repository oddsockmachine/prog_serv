from prog_serv import report
from time import sleep
from random import randint

def main():


    ## start up the server first, leave it running in a separate thread
    ## so it keeps going to program exits
    ## also return a wrapper function
    # results_server, wrapper = start_results_server()

    ## when the wrapper is used, it registers this iterable with the server
    ## its progress is available @ /name
    ## all progress bars are available @ /
    # for x in wrapper(list, name):
    #    do(x)

    todo = xrange(5)

    for _t in report(todo, "1"):
        print _t
        sleep((randint(1,10)/float(5.5)))

    sleep(3)

    todo2 = (x for x in range(5))
    for _t in report(todo2, "2"):
        print _t
        sleep((randint(1,10)/float(5.5)))
    return


if __name__ == "__main__":
    main()